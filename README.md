# README #

Sample Todo App.

### Set Up The Application ###

* Install and configure Docker in your local machine
* Checkout the git repo and run <span style="color: #FF9900">npm install</span> inside todo-app-api folder
* run <span style="color: #FF9900">docker-compose up --build</span> inside todo-app-persistent to build application and run it for the first time.

### Connect to Database ###

* Install PgAdmin 
* Open docker-compose.yml file and under its pgsql section you can find all the data needed to connect.

### Create Database & Tables ###

* Create a Database in server with the name mentioned in docker-compose.yml
* Then Create a table using bellow SQL
``` sql
CREATE TABLE public.todo
(
id uuid NOT NULL,
name character varying NOT NULL,
created_at timestamp with time zone NOT NULL,
updated_at timestamp with time zone,
description character varying,
due_time timestamp with time zone NOT NULL,
PRIMARY KEY (id)
);

ALTER TABLE IF EXISTS public.todo
OWNER to root;
```

