const express = require('express');
const validateDto = require('../ajv-validator/validate-dto');
const todoSchema = require('../ajv-validator/todo-schema');

const todoGetAll = require('../implementation/todo-get-all');
const todoGetById = require('../implementation/todo-get-by-id');
const todoCreate = require('../implementation/todo-create');
const todoDelete = require('../implementation/todo-delete');
const todoUpdate = require('../implementation/todo-update');
const todoExternal = require('../implementation/todo-external');

const router = express.Router();

router.post('/todo', validateDto(todoSchema) , todoCreate.createTodo);

router.delete('/todo/:id', todoDelete.deleteTodo);

router.get('/todo/:id', todoGetById.getByIdTodo);

router.put('/todo/:id', validateDto(todoSchema), todoUpdate.updateTodo);

router.get('/todos', todoGetAll.getAllTodo);

router.get('/todos/external', todoExternal.getAllWithExternalTodo);

module.exports = router;