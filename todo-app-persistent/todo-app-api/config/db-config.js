const { Sequelize } = require('sequelize');
/*
const Pool = require('pg').Pool;
exports.pool = new Pool({
  user: 'root',
  host: '100.20.0.10',
  database: 'todo-app',
  password: 'password',
  port: 5432,
});
*/
const dbConfig = {
  user: 'root',
  host: '100.20.0.10',
  database: 'todo-app',
  password: 'password',
  port: 5432,
  type: 'postgres'
}

const sequelize = new Sequelize(
  dbConfig.database,
  dbConfig.user,
  dbConfig.password,
  {
    host: dbConfig.host,
    port: dbConfig.port,
    dialect: dbConfig.type,
    define: {
      freezeTableName: true
    }
  }
);

module.exports = sequelize;