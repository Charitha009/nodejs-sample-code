const Todo = require('../db/models/todo-model');

exports.delete = async (id) => {
    return await Todo.destroy({
        where: {
            id: id
        }
    });
}

exports.deleteAll = async () => {
    return await Todo.destroy(
        { truncate: true }
    );
}
