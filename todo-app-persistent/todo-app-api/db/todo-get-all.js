const Todo = require('../db/models/todo-model');

exports.getAllWithPagination = async (pagination) => {
    return await Todo.findAndCountAll({
        limit: pagination.limit,
        offset: pagination.offset
    });
}

exports.getAll = async () => {
    return await Todo.findAndCountAll();
}