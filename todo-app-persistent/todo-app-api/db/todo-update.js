const Todo = require('../db/models/todo-model');

exports.update = async (id, updatedTodo) => {
    return await Todo.update(
        updatedTodo,
        {
        where: {
            id: id
        }
    });
}