const { UUIDV4 } = require('sequelize');
const Sequelize = require('sequelize');

const sequelize = require('../../config/db-config');

const Todo = sequelize.define('todo',
    {
        id: {
            type: Sequelize.UUIDV4,
            autoIncrement: UUIDV4,
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: Sequelize.STRING,
            allowNull: false,
            length: 25
        },
        description: {
            type: Sequelize.STRING,
            allowNull: true,
            default: null,
            length: 100
        },
        due_time: {
            type: Sequelize.DATE,
            allowNull: false
        },
        created_at: {
            type: Sequelize.DATE,
            allowNull: false
        },
        updated_at: {
            type: Sequelize.DATE,
            allowNull: true,
            default: null
        }
    },
    {
        timestamps: false,
    },
);

module.exports = Todo;