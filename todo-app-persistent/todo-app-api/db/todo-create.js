const Todo = require('../db/models/todo-model');

exports.create = async (newTodo) => {
    return await Todo.create(newTodo);
}