const express = require('express');
const bodyParser = require('body-parser');

const todoRoutes = require('./routes/todo');

const app = express();

app.use(bodyParser.json());

app.use('/v1', todoRoutes);

app.listen(3000);