const ajv = require('../config/ajv-config');

const dateNow = new Date().toISOString();

const schema = {
    type: "object",
    properties: {
        name: {
          type: "string", 
          maxLength: 25
        },
        description: {
          type: "string", 
          maxLength: 100
        },
        due_time: {
          type: "string",
          format: "date-time",
          formatMinimum: dateNow
        },
    },
    required: ["name", "due_time"],
    additionalProperties: false
  }

module.exports = ajv.compile(schema);