const deleteTodo = require('../db/todo-delete');

exports.deleteTodo = async (req, res, next) => {
    const id = req.params.id;
    if (id.trim().toLowerCase() == 'all') {
      try {
        const result = await deleteTodo.deleteAll();
        res.status(200).json({
          message: 'All Items deleted',
        });
      } catch (err) {
        res.status(500).json({
          message: err.message
        });
      }
    } else {
      try {
        const result = await deleteTodo.delete(id);
        if (result === 1) {
          res.status(200).json({
            message: 'Item deleted',
          });
        } else{
          res.status(400).json({
            message: 'No Item Found',
          });
        }
      } catch (err) {
        res.status(500).json({
          message: err.message
        });
      }
     
    }
  };