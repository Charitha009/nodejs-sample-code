const updateTodo = require('../db/todo-update');
const getByIdTodo = require('../db/todo-get-by-id');

exports.updateTodo = async (req, res, next) => {
  const id = req.params.id;
  try {
    const currentTodo = await (await getByIdTodo.getById(id));
    if (currentTodo !== null) {
      const updatedTodo = {
        id: currentTodo.id,
        name: req.body.name,
        description: req.body.description,
        due_time: req.body.due_time,
        created_at: currentTodo.created_at,
        updated_at: (new Date()).toISOString()
      }
  
      const result = await updateTodo.update(id, updatedTodo);
      res.status(200).json({
        data: updatedTodo,
        meta: {
          message: 'Items Updated',
        }
      });
    } else {
      res.status(400).json({
        message: 'No Item Found',
      });
    }    
  } catch (err) {
    res.status(500).json({
      message: err.message
    });
  }
};