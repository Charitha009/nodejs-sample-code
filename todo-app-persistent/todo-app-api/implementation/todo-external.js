const axios = require('axios');
const getAllTodo = require('../db/todo-get-all');

exports.getAllWithExternalTodo = async (req, res, next) => {
    try {
        const response = await Promise.all(
            [
                axios.get('https://xzax0uu489.execute-api.us-east-1.amazonaws.com/todo-stage'),
                getAllTodo.getAll()
            ]
        );
        res.status(200).json({
            message: 'SUCCESS',
            data: {
                external: response[0].data.data,
                internal: response[1].rows                
            }
        });
    } catch (err) {
        res.status(500).json({
            message: err.message,
        });
    }
}