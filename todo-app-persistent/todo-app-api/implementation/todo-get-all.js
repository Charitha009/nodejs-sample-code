const getAllTodo = require('../db/todo-get-all');

exports.getAllTodo = async (req, res, next) => {
  const limit = +req.query.limit;
  const page = +req.query.page === 0 ? 0 : +req.query.page - 1;
  let offset = 0;
  if (page == 1) {
    offset = page - 1 + limit;
  } else if (page > 1) {
    offset = limit * page;
  }
  const pagination = {
    limit: limit,
    offset: offset
  };
  try {
    const results = await getAllTodo.getAllWithPagination(pagination);
    console.log(results);
    res.status(200).json({
      data: results.rows,
      meta: {
        message: results.count === 0 ? 'No Itms Found' : 'Items Found',
        recoard_count: results.count,
        page_count: Math.ceil(results.count / limit),
        page: page,
        limit: limit,
      }
    });
  }
  catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};