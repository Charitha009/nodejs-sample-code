const { v4: uuidv4 } = require('uuid');
const createTodo = require('../db/todo-create');

exports.createTodo = async (req, res, next) => {
    const newTodo = {
      id: uuidv4(),
      name: req.body.name,
      description: req.body.description,
      due_time: req.body.due_time,
      created_at: (new Date()).toISOString()
    }
    try {
      const result = await createTodo.create(newTodo);
      res.status(200).json({
        data: result,
        meta: {
          message: 'Items Created',
        }
      });
    }
    catch (err) {
      res.status(500).json({
        message: err.message,
      });
    }
  };