const getTodoById = require('../db/todo-get-by-id');

exports.getByIdTodo = async (req, res, next) => {
  const id = req.params.id;
  try {
    const result = await getTodoById.getById(id);
    if (result !== null) {
      res.status(200).json({
        data: result,
        meta: {
          message: 'Items Found',
        }
      });
    } else {
      res.status(400).json({
        message: 'No Item Found',
      });
    }
  }
  catch (err) {
    res.status(500).json({
      message: err.message,
    });
  }
};