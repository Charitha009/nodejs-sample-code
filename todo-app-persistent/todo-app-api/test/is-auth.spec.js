const expect = require('chai').expect;
const sinon = require('sinon');
const isAuth = require('../middleware/is-auth');
const jwt = require('jsonwebtoken');

describe('Auth Middleware',  function() {
    it ('Should throw error if Authorization header not present in the request', function() {
        const req = {
            get: function(headerName) {
                return null;
            }
        }
    
        expect(isAuth.bind(this, req, {}, () => {})).to.throw('Not Authenticated');
    });
    
    it ('Should throw error if Authorization header is only one string', function() {
        const req = {
            get: function(headerName) {
                return 'aabbcc';
            }
        }
        expect(isAuth.bind(this, req, {}, () => {})).to.throw();
    })
    
    it ('Should yield a userId after decoding the token', function() {
        const req = {
            get: function(headerName) {
                return 'Bearer sdsdsfasdfdf';
            }           
        }
        sinon.stub(jwt, 'verify');
        jwt.verify.returns({userId: 'abc'});
        isAuth(req, {}, () => {});
        expect(req).to.have.property('userId');
        expect(req).to.have.property('userId', 'abc');
        expect(jwt.verify.called).to.be.true;
        jwt.verify.restore();
    })

    it ('Should trow error if token not get decoded', function() {
        const req = {
            get: function(headerName) {
                return 'Bearer sdsdsfasdfdf';
            }           
        }
        expect(isAuth.bind(this, req, {}, () => {})).to.throw('jwt malformed');
    })
})